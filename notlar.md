# Notlar
---
## Genel Bilgiler

**Server altında kullandığımız yazılımlar**
* http deamon
* apache PHP Cgi
* IIS (Internet Information Services) Asp Asp.net
* Nginx
* Litespeed 
* litehttp
* Nodejs

## Php temeller
---
### Değişkenler
* Değişkenler,tanımlamalarında herhangi bir değişken tipi belirtmeye gerek yoktur. $değişken adı = veri;_ şeklinde tanımlamalar yapılır.
```php
$deneme_degisken=1234;
```
* Değişkenler kesinlikle sayısal bir ifade ile başlayamaz;
```php
$80osmaniye="il" 
```
* Özel karakterler içeremez */-' gibi,sadece _ kullanılabilir.
* Değişkenler büyük üçük harf duyarlıdır
* . ile değikenler birbirine bağlanabilir
* Değişkenlerin türlerine gettype() fonksiyonu ile ulaşabiliriz
* Değişkenin türünü settype() fonksiyonu ile değiştirebiliriz
* Sabit değişken belirlemek için "define()" fonksiyonu kullanılır

```php
    define("sayi", 2 );
```
### Operatörler
* Atama operatörü >> "="
* Aritmetik operatörler >> " + - * / %(mod)" 
* Artırma operatörleri, degisken++ yada ++degisken şeklinde kullanılır

* _**Bileşik Atama Operatörleri**_
    Standart atamalar ile yapılan atamalara alternatif olarak 
    ```php
        $sayi=3;
        echo $sayi=$sayi+5;
        //yerine
        echo $sayi +=5;
        //kullanılabilir
    ```
* **Karşılaştırma Operatörleri** 
    * == eşitmi,!= eşit değil, === denk ifadesi, < küçüktür,> büyüktür, <= küçük eşit,>= büyük eşit 

* == ifadesi içeriği aynı olan ancak tipleri farklı olan değişkenleri birbirine eşitleyeblir bu durumlarda gerçek eşitliği bulmak için === ( denk ) ifadesini kullanmak gereklidir

### Matematiksel fonksiyonlar

**abs |mutlak değer| Fonksiyonu**
```php
    echo abs($degisken);
```

**ceil (ondalık sayıları üste yuvarlama)**
```php
    echo ceil($degisken);
```
**round sayıyı en yakın ondalık sayıya yuvarlama**
```php
    echo round($degisken);
```
**pow sayıların üssünü almak**
```php
    echo pow($degisken,kuvvet);
```
**max ve min fonksiyonları iki sayıdan en büyük ve en küçük sayıyı verir (aralığı içermez)**
```php
    echo max($degisken,$degisken2);
    echo min($degisken,$degisken2);
```
**sqrt sayıların karekökünü almak**
```php
    echo sqrt($degisken);
```
**sin cos tan fonksiyonları**
```php
    echo cos($degisken);
    echo tan($degisken);
    echo sin($degisken);

```

### Karşılaştırma Operatörleri 

* == != < > <= >= şeklinde tanımlanır 
* Mantıksal bağlaçlar  &&(ve) ||(veya) ve-veya bağlaçlaro or and şeklinde de kullanılabilir.

* **If Cümleciği** karşılaştırma alarak çalıştırılmak istenen kod bloğunu oluşturmak için kullanılır

## Döngüler

```php
// for döngüsü
for ($i=; $i<100;$i++){
    echo $i;
}

// while döngüsü

while (sorgu){

    işlem
}
```

### Metinsel fonksiyonlar

* **Wordwrap** sözcük kaydırma fonksiyonu
```php
echo wordwrap($metin,9,"<br>",true);
```
* **ucwords** verilen kelimelerin ilk karakterlerini büyük yapar
* **ucfirst** verilen metnin sadece ilk harfini büyük yapar
* **trim** kelimenin sağında yada solanda alan boşlukları siler